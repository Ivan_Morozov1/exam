package exam;
import java.io.*;
import java.util.Scanner;

public class Directory {
    static Scanner scn = new Scanner(System.in);
    public static void main(String[] args) {
        File file = new File(scn.nextLine());
        File[] files = file.listFiles();
        printFiles(files);
        System.out.println("src/exam" + file.getAbsolutePath());
    }

    private static void printFiles(File[] files) {
        File[] innerFiles;
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    innerFiles = file.listFiles();
                    printFiles(innerFiles);
                } else {
                    System.out.println(file);
                }
            }
        }
    }
}