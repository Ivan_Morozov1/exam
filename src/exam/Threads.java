package exam;

public class Threads{
    public static void main(String[] args) {
        Thread1 thread1 = new Thread1();
    Thread2 thread2 = new Thread2();
    thread1.start();
    thread2.start();
    }
}
class Thread1 extends Thread{
    @Override
    public void run(){
        System.out.println("Поток 1");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
class Thread2 extends Thread{
    @Override
    public void run(){
        System.out.println("Поток 2");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
